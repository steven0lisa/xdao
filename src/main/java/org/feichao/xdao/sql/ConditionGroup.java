package org.feichao.xdao.sql;


/**
 * @author zhangzc@djcars.cn
 * @create 2016/11/21
 */
public interface ConditionGroup extends Condition {

    ConditionGroup and(ConditionGroup g);

    ConditionGroup or(ConditionGroup g);

    ConditionGroup and(String field, String op, Object val);

    ConditionGroup or(String field, String op, Object val);

}
