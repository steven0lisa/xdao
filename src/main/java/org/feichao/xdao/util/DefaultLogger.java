package org.feichao.xdao.util;

import org.feichao.xdao.XDAO;

/**
 * 默认日志实现
 *
 * @author chao
 * @version 2015-05-26
 */
public class DefaultLogger implements XDAO.Logger {

	public void info(String msg) {
		System.out.println("[INFO]" + msg);
	}

	public void warn(String msg) {
		System.out.println("[WARN]" + msg);
	}

	public void error(String msg) {
		System.err.println("[ERR]" + msg);
	}

	public void error(Throwable e, String msg) {
		System.err.println("[ERR]" + msg);
		e.printStackTrace();
	}
}
