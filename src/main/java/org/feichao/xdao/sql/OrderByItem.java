package org.feichao.xdao.sql;

import org.feichao.xdao.util.SQLContext;

/**
 * Order语句
 *
 * @author zhangzc@djcars.cn
 * @create 2016/11/21
 */
public class OrderByItem implements NoParamItem{

    private String name;

    private String by;

    public OrderByItem(String name, String by) {
        this.name = name;
        this.by = by;
    }

    void buildSql(SQLContext context, StringBuilder sql){
        sql.append(" ").append(name).append(" ").append(by);
    }
}
