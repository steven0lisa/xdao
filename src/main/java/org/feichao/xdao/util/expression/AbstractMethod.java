package org.feichao.xdao.util.expression;

/**
 * SQL语句的方法父类
 *
 * @author chao
 * @since 2015/6/30
 */
public abstract class AbstractMethod {

    public abstract Object invoke();
}
