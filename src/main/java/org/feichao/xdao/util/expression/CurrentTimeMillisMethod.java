package org.feichao.xdao.util.expression;

import org.feichao.xdao.annotations.MethodName;

/**
 * 提供当前时间的毫秒
 *
 * @author chao
 * @since 2015/6/30
 */
@MethodName("now")
public class CurrentTimeMillisMethod extends AbstractMethod {

    @Override
    public Object invoke() {
        return System.currentTimeMillis();
    }
}
