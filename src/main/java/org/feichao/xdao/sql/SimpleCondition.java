package org.feichao.xdao.sql;

import org.feichao.xdao.util.SQLContext;

/**
 * @author zhangzc@djcars.cn
 * @create 2016/11/21
 */
public class SimpleCondition implements Condition, ParamItem {

    private String field;
    private String op;
    private Object val;

    public SimpleCondition(String field, String op, Object val) {
        this.field = field;
        this.op = op;
        this.val = val;
    }

    public Object getParam() {
        return val;
    }

    public void buildSql(SQLContext context, StringBuilder sql) {
        sql.append(String.format(" (%s %s ?) ", field, op));
    }
}
