package org.feichao.xdao.sql;

import org.feichao.xdao.util.SQLContext;

/**
 * @author zhangzc@djcars.cn
 * @create 2016/11/21
 */
public class Static implements SqlExpression {

    private String str;

    public Static(String str) {
        this.str = str;
    }

    public void buildSql(SQLContext context, StringBuilder sql) {
        sql.append(" " + str + " ");
    }
}
