package org.feichao.xdao.sql;

/**
 * @author zhangzc@djcars.cn
 * @create 2016/11/21
 */
public interface OrderBy {

    OrderBy asc(String name);

    OrderBy desc(String name);


}
