package org.feichao.xdao.test;

import org.feichao.xdao.Dao;
import org.feichao.xdao.XDAO;
import org.feichao.xdao.sql.Cnd;
import org.feichao.xdao.sql.Pager;
import org.feichao.xdao.test.daos.PersonDAO;
import org.feichao.xdao.test.models.Person;
import org.feichao.xdao.util.Log;
import org.junit.BeforeClass;
import org.junit.Test;

import java.sql.Connection;
import java.sql.DriverManager;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;

/**
 * @author chao
 * @since 2015/5/26
 */
public class XDAOTest {


    @BeforeClass
    public static void prepareDbServer(){
        XDAO.DataSource ds  = new XDAO.DataSource() {
            public Connection getConnection() {
            try {
                Class.forName("org.h2.Driver");
                return DriverManager.getConnection("jdbc:h2:mem:play;MODE=MYSQL", "sa", "");
            } catch (Exception e) {
                e.printStackTrace();
            }
            return null;
            }
        };

        XDAO.setDataSource(ds);
        XDAO.debug = true;

        PersonDAO dao = XDAO.get(PersonDAO.class);
        dao.createPersonTable();
        dao.createPersonAgeTable();

        dao.insertPerson("abc", "steven");
        dao.insertPersonAge("1", "abc", 26);

        dao.insertPerson("def", "lisa");
        dao.insertPersonAge("2", "def", 18);

        dao.insertPerson("ghi", "jenny");
        dao.insertPersonAge("3", "ghi", 26);
    }

    @Test
    public void testDao() {
        PersonDAO dao = XDAO.get(PersonDAO.class);
        assertNotNull(dao.findById("abc"));
        List<Person> persons = dao.findAll();
        assertFalse(persons.isEmpty());
        assertTrue(dao.countAll() > 0);
        assertNotNull(dao.findByIdAndName("abc", "steven"));
        assertEquals(18, dao.findPersonAgeByName("lisa").age);
        Person person = new Person();
        person.id = "id";
        person.name = "name";
        dao.insertPersonObject(person);
        assertNotNull(dao.findById(person.id));
    }

    @Test
    public void testBatch(){
        PersonDAO dao = XDAO.get(PersonDAO.class);
        List<Person> persons = new ArrayList<Person>();
        for (int i = 0; i < 5; i++) {
            Person person = new Person();
            person.id = "id_" + i;
            person.name = "name_" + i;
            persons.add(person);
        }
        dao.batchInsert(persons);
        for (int i = 0; i < 5; i++) {
            assertNotNull(dao.findById("id_" + i));
        }
    }

    @Test
    public void testMarco(){
        PersonDAO dao = XDAO.get(PersonDAO.class);
        assertNotNull(dao.findByTableAndName("person", "steven"));
    }

    @Test
    public void testMethod(){
        PersonDAO dao = XDAO.get(PersonDAO.class);
        Person person = dao.findByTableAndName("person", "steven");
        assertNotSame(0l, person.createTime);
    }

    @Test
    public void testStringList(){
        PersonDAO dao = XDAO.get(PersonDAO.class);
        List<String> ids = dao.findAllId();
        for(String id: ids){
            assertTrue(id != null && !"".equals(id));
        }
        assertTrue(!ids.isEmpty());
    }

    @Test
    public void testSQLIn(){
        PersonDAO dao = XDAO.get(PersonDAO.class);
        List<String> ids = new ArrayList();
        ids.add("abc");
        ids.add("def");
        long size = dao.countByUserIds(ids);
        Log.info("SIZE = " + size);
        assertTrue(size == 2);
    }

    @Test
    public void testModelDao(){
        Dao<Person> dao = XDAO.getDao(Person.class);
        Person person = dao.fetchById("abc");
        assertNotNull(person);
    }

    @Test
    public void testCnd(){
        PersonDAO dao = XDAO.get(PersonDAO.class);
        Cnd cnd = Cnd.where("name", "=", "steven").or("name", "=", "steven");
        cnd.orderBy().asc("name");
        assertNotNull(dao.findByCnd(cnd, null));

        dao = XDAO.get(PersonDAO.class);
        cnd = Cnd.create().and("name", "=", "steven").or("name", "=", "steven");
        cnd.orderBy().asc("name");

        assertNotNull(dao.findCreateTimeNotEmptyByCnd(cnd, Pager.get(dao.countAll())));
    }
}
