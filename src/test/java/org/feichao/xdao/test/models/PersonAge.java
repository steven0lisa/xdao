package org.feichao.xdao.test.models;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author chao
 * @version 2015-05-27
 */
@Table(name="person_age")
public class PersonAge {

	@Id
	public String id;

	@Column(name="person_id")
	public String personId;

	public int age;

}
