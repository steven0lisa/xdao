package org.feichao.xdao.sql;

import org.apache.commons.lang.StringUtils;
import org.feichao.xdao.util.SQLContext;

import java.util.LinkedList;
import java.util.List;

/**
 * 条件
 *
 * @author zhangzc@djcars.cn
 * @create 2016/11/21
 */
public class Cnd implements ConditionGroup, OrderBy {

    public List<SqlExpression> expressionList = new LinkedList<SqlExpression>();

    public List<OrderByItem> orderByList = new LinkedList<OrderByItem>();

    public static Cnd create(){
        Cnd c = new Cnd();
        return c;
    }

    public static Cnd whereStatic(String condition){
        Cnd c = new Cnd();
        c.expressionList.add(new Static(condition));
        return c;
    }

    public static Cnd whereAnd(String field, String op, Object val){
        Cnd c = new Cnd();
        c.expressionList.add(new Static("AND"));
        c.expressionList.add(new SimpleCondition(field, op, val));
        return c;
    }

    public static Cnd whereOr(String field, String op, Object val){
        Cnd c = new Cnd();
        c.expressionList.add(new Static("OR"));
        c.expressionList.add(new SimpleCondition(field, op, val));
        return c;
    }

    public static Cnd where(String field, String op, Object val){
        Cnd c = new Cnd();
        c.expressionList.add(new SimpleCondition(field, op, val));
        return c;
    }

    public Cnd and(ConditionGroup g) {
        expressionList.add(new Static("AND"));
        expressionList.add(g);
        return this;
    }

    public Cnd or(ConditionGroup g) {
        expressionList.add(new Static("OR"));
        expressionList.add(g);
        return this;
    }

    public Cnd and(String field, String op, Object val) {
        expressionList.add(new Static("AND"));
        expressionList.add(new SimpleCondition(field, op, val));
        return this;
    }

    public Cnd or(String field, String op, Object val) {
        expressionList.add(new Static("OR"));
        expressionList.add(new SimpleCondition(field, op, val));
        return this;
    }

    public OrderBy asc(String name) {
        OrderByItem item = new OrderByItem(name, "ASC");
        orderByList.add(item);
        return this;
    }

    public OrderBy desc(String name) {
        OrderByItem item = new OrderByItem(name, "DESC");
        orderByList.add(item);
        return this;
    }

    public OrderBy orderBy() {
        return this;
    }


    public void buildSql(SQLContext context, StringBuilder sql) {
        if(expressionList.isEmpty()){
           return;
        }
        if(!StringUtils.containsIgnoreCase(sql.toString(), "where")){
            sql.append(" WHERE ");
        }

        for(SqlExpression e: expressionList){
            e.buildSql(context, sql);
        }
        if(orderByList.isEmpty()){
            return;
        }
        sql.append(" ORDER BY ");
        for(OrderByItem e: orderByList){
            e.buildSql(context, sql);
            sql.append(',');
        }
        sql.deleteCharAt(sql.length() - 1);

    }
}
