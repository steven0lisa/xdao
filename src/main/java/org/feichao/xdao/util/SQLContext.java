package org.feichao.xdao.util;

import org.feichao.xdao.XDAO;
import org.feichao.xdao.sql.Cnd;
import org.feichao.xdao.sql.PageInfo;

import java.util.LinkedHashMap;
import java.util.Map;

/**
 * SQL模版运行的上下文
 *
 * @author chao
 * @version 2015-05-25
 */
public class SQLContext {

    /**
     * SQL模版
     */
	public String sqlTemplate;

    /**
     * 是否批量执行
     */
    public boolean isBatch = false;

    /**
     * 数据源名称
     */
    public String dataSourceName = XDAO.DefaultDataSourceName;

    /**
     * 方法参数
     */
	public Map<String, Object> params = new LinkedHashMap<String, Object>();

    /**
     * 条件
     */
    public Cnd cnd = null;

    /**
     * 分页
     */
    public PageInfo pageInfo = null;

}
