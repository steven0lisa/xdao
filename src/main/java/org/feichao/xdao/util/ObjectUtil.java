package org.feichao.xdao.util;

import org.apache.commons.lang.StringUtils;
import org.feichao.xdao.XDAO;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.PersistenceUnit;
import javax.persistence.Table;
import java.lang.annotation.Annotation;
import java.lang.reflect.Field;
import java.lang.reflect.Method;
import java.util.ArrayList;
import java.util.List;

/**
 * @author zhangzc@djcars.cn
 * @create 2016/11/4
 */
public class ObjectUtil {

    public static Field getFieldByAnnotation(Class clazz, Class<? extends Annotation> anno){
        Field[] fields = clazz.getFields();
        for(Field f: fields){
            if(f.isAnnotationPresent(anno)){
                return f;
            }
        }
        return null;
    }

    public static List<Field> getFieldsByAnnotation(Class clazz, Class<? extends Annotation> anno){
        List fieldList = new ArrayList();
        Field[] fields = clazz.getFields();
        for(Field f: fields){
            if(f.isAnnotationPresent(anno)){
                fieldList.add(f);
            }
        }
        return fieldList;
    }

    public static String extractDbId(Class clazz){
        Field idField = getFieldByAnnotation(clazz, Id.class);

        if(idField != null){
            String name = idField.getName();
            if(idField.isAnnotationPresent(Column.class)){
                Column column = idField.getAnnotation(Column.class);
                if(StringUtils.isNotEmpty(column.name())){
                    name = column.name();
                }
            }
            return name;
        }
        return "id";
    }

    public static String extractDbTable(Class clazz){
        if(clazz.isAnnotationPresent(Table.class)){
            Table tableAnno = (Table) clazz.getAnnotation(Table.class);
            if(StringUtils.isNotEmpty(tableAnno.name())){
                return tableAnno.name();
            }else{
                return clazz.getName();
            }
        }else{
            return clazz.getName();
        }

    }

    public static String extractDataSource(Class clazz){
        if(clazz.isAnnotationPresent(PersistenceUnit.class)){
            PersistenceUnit persistenceUnit = (PersistenceUnit) clazz.getAnnotation(PersistenceUnit.class);
            if(StringUtils.isNotEmpty(persistenceUnit.name())){
                return persistenceUnit.name();
            }else{
                return XDAO.DefaultDataSourceName;
            }
        }else{
            return XDAO.DefaultDataSourceName;
        }
    }

    public static String extractDataSource(Method method){
        PersistenceUnit persistenceUnit = method.getAnnotation(PersistenceUnit.class);
        if(persistenceUnit == null){
            persistenceUnit = method.getDeclaringClass().getAnnotation(PersistenceUnit.class);
        }
        if(persistenceUnit != null){
            if(StringUtils.isNotEmpty(persistenceUnit.name())){
                return persistenceUnit.name();
            }else{
                return XDAO.DefaultDataSourceName;
            }
        }else{
            return XDAO.DefaultDataSourceName;
        }
    }
}
