package org.feichao.xdao.sql;

import org.feichao.xdao.util.SQLContext;

/**
 * @author zhangzc@djcars.cn
 * @create 2016/11/21
 */
public interface SqlExpression {

    void buildSql(SQLContext context, StringBuilder sql);
}
