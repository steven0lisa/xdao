package org.feichao.xdao.test.models;

import javax.persistence.Column;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author chao
 * @version 2015-05-26
 */
@Table(name="person")
public class Person {

	@Id
	public String id;

	@Column(name="name")
	public String name;

    @Column(name="create_time")
    public Long createTime;
}
