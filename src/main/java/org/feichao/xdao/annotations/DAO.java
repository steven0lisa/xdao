package org.feichao.xdao.annotations;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

/**
 * 标记DAO接口，标记后的接口配合@SQL可以通过XDAO产生DAO对象
 *
 * @author chao
 * @version 2015-05-25
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.TYPE)
public @interface DAO {

}
