package org.feichao.xdao.util;

import org.feichao.xdao.XDAO;

/**
 * 日志工具
 *
 * @author chao
 * @since 2015/5/26
 */
public class Log {

    public static void info(String msg, Object... args){
        XDAO.getLog().info(String.format(msg, args));
    }

    public static void warn(String msg, Object... args){
        XDAO.getLog().warn(String.format(msg, args));
    }

    public static void error(String msg, Object... args){
        XDAO.getLog().error(String.format(msg, args));
    }

    public static void error(Throwable e, String msg, Object... args){
        XDAO.getLog().error(e, String.format(msg, args));
    }
}
