package org.feichao.xdao.test.daos;

import org.feichao.xdao.XDAO;
import org.feichao.xdao.annotations.DAO;
import org.feichao.xdao.annotations.SQL;
import org.feichao.xdao.annotations.SQLParam;
import org.feichao.xdao.sql.Cnd;
import org.feichao.xdao.sql.PageInfo;
import org.feichao.xdao.test.models.Person;
import org.feichao.xdao.test.models.PersonAge;

import javax.persistence.PersistenceUnit;
import java.util.List;

/**
 * @author chao
 * @version 2015-05-26
 */
@DAO
@PersistenceUnit(name = XDAO.DefaultDataSourceName)
public interface PersonDAO {

	@SQL("create table person(id varchar(255) not null, name varchar(255), create_time bigint(20) not null default 0)")
	void createPersonTable();

	@SQL("create table person_age(id varchar(255) not null, person_id varchar(255) not null, age int default 0)")
	void createPersonAgeTable();

	@SQL("insert into person(id, name, create_time) values(@1, @2, #now )")
	void insertPerson(String id, String name);

    @SQL("insert into person(id, name, create_time) values(@1.id, @1.name, #now )")
    void insertPersonObject(Person person);

	@SQL("insert into person_age(id, person_id, age) values(@1, @2, @3)")
	void insertPersonAge(String id, String personId, int age);

	@SQL("select * from person where id=@1")
	Person findById(String id);

	@SQL("select * from person where id=@id and name=@name")
	Person findByIdAndName(@SQLParam("id")String id, @SQLParam("name")String name);

	@SQL("select * from person")
	List<Person> findAll();

    @SQL("select id from person")
    List<String> findAllId();

	@SQL("select count(*) from person")
	long countAll();

	@SQL("select count(*) from person where id in (:ids)")
	long countByUserIds(@SQLParam("ids") List<String> ids);

	@SQL("select pa.* from person p, person_age pa where p.id=pa.person_id and p.name=@1")
	PersonAge findPersonAgeByName(String name);

    @SQL(value = "insert into person(id, name) values(@1.id, @1.name)", isBatch = true)
    int[] batchInsert(List<Person> persons);

    @SQL("select * from $1 where name = @2")
	@PersistenceUnit(name = XDAO.DefaultDataSourceName)
    Person findByTableAndName(String tableName, String name);

	@SQL("select * from person")
	Person findByCnd(Cnd cnd, PageInfo pageInfo);

	@SQL("select * from person where create_time > 0")
	Person findCreateTimeNotEmptyByCnd(Cnd cnd, PageInfo pageInfo);
}
